## Pinger

Simple service that will ping a service of your choice.

### Requirements

- Golang
- Docker

### Setup

#### Locally

```
make dep build
```

Binary will be available in the `bin` folder.

#### Docker

```
make docker_image 
```

Image `devops/pinger` being built, and may be run through the command:

```
docker run --rm devops/pinger
```

### Environment Variables

| Name         | Description                                       | Default     |
|--------------|---------------------------------------------------|-------------|
| INTERFACE    | Network interface to listen onto.                 | "0.0.0.0"   |
| PORT         | Port to listen from.                              | 8000        |
| TARGET_PROTO | Network protocol to use.                          | "http"      |
| TARGET_HOST  | Host to send the ping request to.                 | "localhost" |
| TARGET_PORT  | Port of the target host to send the request to.   | 8000        |
| TARGET_PATH  | URI of the target host the ping shall be sent to. | "/"         |

### Usage Examples

#### Local

```
TARGET_HOST=1.1.1.1 TARGET_PORT=1234 ./bin/pinger
```

#### Docker

```
docker run --rm -e TARGET_HOST=1.1.1.1 -e TARGET_PORT=1234 devops/pinger
```